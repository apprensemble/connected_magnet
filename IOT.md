# IoT

## Qu'est-ce que c'est?

IoT pour Internet of things ou encore objet connectés en français. On peut les considerer aujourd'hui comme de minis ordi connectés, muni de capteur et n'ayant qu'un minimum de tache à accomplir. 

## Cas d'usage

Relevé et suivi de temperature, CO2, comportement.
Plus largement cela peut servir pour la sécurité(nationale et domestique), pour le confort à la maison ou en extérieur, la logistique, la recherche, etc...

# Connected Magnet

## Qu'est-ce que c'est?

Pour cet atelier j'ai choisi de faire un aimant connecté. Il se positionne quelque par à la maison par exemple sur le réfrigérateur et permet de recevoir des messages, tel que une liste de course, un rendez vous à ne pas manquer, une info à retenir. Afin de garder l'exemple simple ce se limite à la reception de méssage mais on aurait pu ajouter la méteo, l'heure du prochain tram, la température de la pièce, un pomodoro, un sélectionneur de recette de cuisine au hasard :) Après cet atelier, vous pourrez en faire ce que vous voudrez.

## Architecture

Pour envoyer et recevoir des messages venant de l'extérieur de la maison il nous faut une infrastructure. Un ensemble de logiciel et matériel permettant aux messages de circuler. Pour notre aimant j'ai choisie :

* Un point d'accès WIFI connecté à internet (par exemple votre BOX)
* Un outil capable prendre en charge des messages et de nous notifier lorsqu'ils arrivent(MQTT)
* Une interface de contrôle permettant de nous notifier depuis l'extérieur(Une WebApp). 
* Et bien entendu notre objet connecté(ESP8266)

## Langage et programme

En générale les programmes sont fait en C mais il est aujourd'hui possible d'utiliser beaucoup de langages plus populaire et accessible tel que Python, Ruby et même java sur certains micro contrôleurs.

Pour ce projet j'ai choisi Lua. C'est un langage de programmation minuscule. Complet il ne prend que quelques d'espace disque. Il est simple leger et facile à interfacer avec le C. Il a été fait par des moines. Original non?

# Utilisation


## Comment l'utiliser

Une fois que nous aurons mis en place l'infra puis monté et mis en fonctionnement notre objet connecté. Pour l'utiliser il suffira de se connecter à votre WebApp oui j'ai bien dis votre WebApp de choisir l'expediteur écrire votre message et envoyer. Simple non?

# Extensions

## Horraire du prochain tram

Si nous avons le temps je vous montrerais comment récuperer les horraires du prochain tram de la tam. Sinon scruté mon gitlab je trouverais un moyen simple de le transmettre. 

## Quelques autres exemples

Comme dis plus haut, la météo, le suivi de points de surveillance, le trafic...

# Droits d'auteurs

Ce projet est sous copyleft. Ça veut dire que vous pouvez en faire ce que vous souhaitez à condition de me citer. 

Thierry V. Apprensemble
