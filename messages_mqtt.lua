-- Quick n dirty just put things together
-- TODO : split lines, bigger font size, control max size
-- init variable ecran
sda = 3 -- SDA Pin
scl = 4 -- SCL Pin
id = 0
message = "1234567890123456789012"
provenance = "from who?"
date = "aujourd'hui"

-- fonction donnant date et heure
function what_time() 
  tm = rtctime.epoch2cal(rtctime.get())
  return string.format("%04d/%02d/%02d %02d:%02d:%02d", tm["year"], tm["mon"], tm["day"], tm["hour"]+2, tm["min"], tm["sec"])
end

function init_OLED(sda,scl) --Set up the u8glib lib
     sla = 0x3C
     i2c.setup(id, sda, scl, i2c.SLOW)
     -- disp = u8g2.ssd1306_128x64_i2c(sla)
		 disp = u8g2.ssd1306_i2c_128x64_noname(id, sla)
     disp:setFont(u8g2.font_6x10_tf) -- about 6 lines & 21 char by line
     disp:setFontRefHeightExtendedText()
     disp:setFontPosTop()
     --disp:setRot180()           -- Rotate Display if needed
end

function write_OLED(topic,date,data) -- Write Display
     --disp:drawFrame(2,2,126,62)
		 disp:clearBuffer()
     disp:drawStr(5, 0, topic)
     -- disp:drawStr(5, 7, str1)
     -- disp:drawStr(40, 30,  string.format("%02d:%02d:%02d",h,m,s))
     -- disp:drawStr(5, 15, destinataire)
     disp:drawStr(0, 15, data)
     disp:drawStr(0, 35, date)
     -- disp:drawStr(0, 50, provenance)
		 disp:sendBuffer()
     --disp:drawXBM(0, 0, 128, 64, xbm_data)
     --disp:drawXBM( 0, 20, 38, 24, xbm_data )
     --disp:drawCircle(18, 47, 14)
end

-- init mqtt client without logins, keepalive timer 120s
m = mqtt.Client("connected_magnet", 120)

-- init mqtt client with logins, keepalive timer 120sec
m = mqtt.Client("connected_magnet", 120, "ntkiatrc", "M9yNxgX7Dzss")

-- setup Last Will and Testament (optional)
-- Broker will publish a message with qos = 0, retain = 0, data = "offline"
-- to topic "/lwt" if client don't send keepalive packet
m:lwt("/lwt", "offline", 0, 0)

m:on("connect", function(client) print ("connected") end)
m:on("offline", function(client) print ("offline") end)

-- on publish message receive event
m:on("message", function(client, topic, data)
	date = what_time()
  print(topic .. ":" .. date)
  if data ~= nil then
		message = data
    print(message)
		write_OLED(topic,date,message)
  end
end)

-- on publish overflow receive event
-- m:on("overflow", function(client, topic, data)
--  print(topic .. " partial overflowed message: " .. data )
-- end)

-- for TLS: m:connect("192.168.11.118", secure-port, 1)
m:connect("m24.cloudmqtt.com", 14441, 0, function(client)
  print("connected")
  -- Calling subscribe/publish only makes sense once the connection
  -- was successfully established. You can do that either here in the
  -- 'connect' callback or you need to otherwise make sure the
  -- connection was established (e.g. tracking connection status or in
  -- m:on("connect", function)).

  -- subscribe topic with qos = 0
	-- add a suscriber for each user
  client:subscribe("Max", 0, function(client) print("subscribe success") end)
  client:subscribe("Celeste", 0, function(client) print("subscribe success") end)
  -- publish a message with data = hello, QoS = 0, retain = 0
  client:publish("Celeste", message, 0, 0, function(client) print("sent") end)
end,
function(client, reason)
  print("failed reason: " .. reason)
end)
m:close();
-- you can call m:connect again
init_OLED(sda,scl)
sntp.sync()
