-- setup SPI and connect display
-- C'est du hspi et non du spi
message = "1234567890123456789012;"
provenance = "from who?"
date = "aujourd'hui"
function init_spi_display()
	-- Hardware SPI CLK  = GPIO14
	-- Hardware SPI MOSI = GPIO13 == 7 parce que hmosi
	-- Hardware SPI MISO = GPIO12 (not used)
	-- Hardware SPI /CS  = GPIO15 (not used)
	-- CS, D/C, and RES can be assigned freely to available GPIOs
	local cs  = 8 -- GPIO15, pull-down 10k to GND
	local dc  = 4 -- GPIO2
	local res = 0 -- GPIO16

	-- en resumer parce que franchement c'est pas clair :
	-- ecran -->ESP
	-- 5V    --> 3.3V
	-- GND	 --> GND
	-- NA    --> NA veut dire non utilisé
	-- NA    --> NA
	-- NA    --> NA
	-- LED   --> 3.3V
	-- SCL   --> D5
	-- SDA   --> D7
	-- RS    --> D4
	-- RST   --> D0
	-- CS    --> D8
	--
	spi.setup(1, spi.MASTER, spi.CPOL_LOW, spi.CPHA_LOW, 8, 8)
	-- we won't be using the HSPI /CS line, so disable it again
	gpio.mode(8, gpio.INPUT, gpio.PULLUP)

	-- initialize the matching driver for your display
	-- see app/include/ucg_config.h
	--disp = ucg.ili9341_18x240x320_hw_spi(cs, dc, res)
	disp = ucg.st7735_18x128x160_hw_spi(cs, dc, res)
end

function clearScreen()
	disp:begin(ucg.FONT_MODE_TRANSPARENT)
	disp:clearScreen()

	disp:setFont(ucg.font_ncenR12_tr);
	disp:setColor(255, 255, 255);
	disp:setColor(1, 255, 0,0);
end

function HelloWorld()
	clearScreen()

	disp:setPrintPos(0, 25)
	disp:print("123456789 123456789 123")
end

-- pas de switch en lua :-(
color = {
	-- les couleurs sont inversé par rapport au web BGR au lieu de RGB
  ["orange"] = function () disp:setColor(51, 118, 220) end,
  ["white"] = function () disp:setColor(255, 255, 255) end,
	["black"] = function () disp:setColor(0, 0, 0) end,
	["blue"] = function () disp:setColor(255, 0, 0) end,
	["aqua"] = function () disp:setColor(255, 255, 0) end,
	["green"] = function () disp:setColor(0, 128, 0) end,
	["teal"] = function () disp:setColor(128, 128, 0) end,
	["lime"] = function () disp:setColor(0, 255, 0) end,
	["fuchsia"] = function () disp:setColor(255, 0, 255) end,
	["purple"] = function () disp:setColor(128, 0, 128) end,
	["yellow"] = function () disp:setColor(0, 255, 255) end,
}

-- split from Peter Prade
function strsplit(delimiter, text)
   local list = {}
   local pos = 1
   if strfind("", delimiter, 1) then -- this would result in endless loops
      error("delimiter matches empty string!")
   end
   while 1 do
      local first, last = strfind(text, delimiter, pos)
      if first then -- found?
         tinsert(list, strsub(text, pos, first-1))
         pos = last+1
      else
         tinsert(list, strsub(text, pos))
         break
      end
   end
   return list
 end

function write_OLED(topic,date,data)
	clearScreen()

	disp:setPrintPos(1, 25)
	color["yellow"]()
	disp:print(date)

	disp:setPrintPos(1, 40)
	color["white"]()
	disp:print(topic)

	disp:setPrintPos(1, 55)
	color["aqua"]()
	x = 55
	line_size = 14
	line_height = 15
	lines = string.len(data) / line_size
	n = 0
	c = 1 -- On met le curseur sur le premier caractere

	while n < lines do
		disp:setPrintPos(1, x)
		disp:print(string.sub(data, c, c +(line_size - 1))) -- on commence au caractere 1 et non zero
		x = x + line_height -- increment hauteur ligne ecran
		n = n + 1 -- increment compteur ligne 
		c = c + line_size -- increment curseur de lecture string
	end
end

function write_OLED_multiline(topic,date,data)
	clearScreen()

	disp:setPrintPos(1, 25)
	color["yellow"]()
	disp:print(date)

	disp:setPrintPos(1, 40)
	color["white"]()
	disp:print(topic)

	disp:setPrintPos(1, 55)
	color["aqua"]()
	x = 55
	line_size = 14
	line_height = 15
	lines = string.len(data) / line_size
	n = 0
	c = 1 -- On met le curseur sur le premier caractere

    local i = 0
    while true do
      i = string.find(data, ";", i+1)    -- find 'next' newline
      if i == nil then break end
			disp:setPrintPos(1, x)
			disp:print(string.sub(data, c, i - 1)) -- on commence au caractere 1 et non zero
			c = i + 1
			x = x + line_height
    end
end

-- fonction donnant date et heure
function what_time() 
	tm = rtctime.epoch2cal(rtctime.get())
	return string.format("%04d/%02d/%02d %02d:%02d:%02d", tm["year"], tm["mon"], tm["day"], tm["hour"]+2, tm["min"], tm["sec"])
end

-- init mqtt client without logins, keepalive timer 120s
-- m = mqtt.Client("connected_magnet", 120)

-- init mqtt client with logins, keepalive timer 120sec
m = mqtt.Client("connected_magnet", 120, MQTT_USERNAME, MQTT_PASSWORD)

-- setup Last Will and Testament (optional)
-- Broker will publish a message with qos = 0, retain = 0, data = "offline"
-- to topic "/lwt" if client don't send keepalive packet
m:lwt("/lwt", "offline", 0, 0)

m:on("connect", function(client) print ("connected") end)
m:on("offline", function(client) print ("offline") end)

-- on publish message receive event
m:on("message", function(client, topic, data)
	date = what_time()
	print(topic .. ":" .. date)
	if data ~= nil then
		message = data
		print(message)
		write_OLED_multiline(topic,date,message)
	end
end)

-- on publish overflow receive event
-- m:on("overflow", function(client, topic, data)
--  print(topic .. " partial overflowed message: " .. data )
-- end)

-- for TLS: m:connect("192.168.11.118", secure-port, 1)
m:connect(MQTT_CLOUD_URL, MQTT_CLOUD_PORT, 0, function(client)
	print("connected")
	-- Calling subscribe/publish only makes sense once the connection
	-- was successfully established. You can do that either here in the
	-- 'connect' callback or you need to otherwise make sure the
	-- connection was established (e.g. tracking connection status or in
	-- m:on("connect", function)).

	-- subscribe topic with qos = 0
	-- add a suscriber for each user
for i,v in ipairs(TOPICS) do
	client:subscribe(v, 0, function(client) print("subscribe success") end)
	client:publish(v, message, 0, 0, function(client) print("sent") end)
end
	-- publish a message with data = hello, QoS = 0, retain = 0
end,
function(client, reason)
	print("failed reason: " .. reason)
end)
m:close();

sntp.sync()
init_spi_display()
HelloWorld()
