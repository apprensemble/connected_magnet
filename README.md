# Preparation

## raccordement ecran LCD

Je vous conseille de le faire après chargement des programmes et du firmware.

	-- ecran --> ESP
	-- 5V    --> 3.3V
	-- GND	 --> GND
	-- NA    --> NA veut dire non utilisé
	-- NA    --> NA
	-- NA    --> NA
	-- LED   --> 3.3V
	-- SCL   --> D5
	-- SDA   --> D7
	-- RS    --> D4
	-- RST   --> D0
	-- CS    --> D8

## Construction et téléchargement du firmware


*Un firmware est un programme necessaire au fonctionnement du matériel et souvent non directement lié à une application. Il peut s'agir d'un systeme d'exploitation, de pilotes, bibliothèques...
Dans le cadre de ce projet le role du firmware est de démarer le matériel et nous fournir un interpreteur lua ainsi que les bibliothèques permettant l'exploitation du matériel en lua. Lua est un langage de programmation. J'ai préconfiguré le firmware pour supporter notre écran, le wifi et la gestion du temps.Il est ensuite automatiquement construit par gitlab-ci.*

**Pour le télécharger :**

Aller sur la page : https://framagit.org/apprensemble/nodemcu-firmware/pipelines?scope=branches&page=1
et cliquer sur le petit nuage à droite de la branche st7735 puis download compile artifacts.
Vous obtiendrez un fichier artifacts.zip

Une fois téléchargé le dezipper il doit contenir un fichier nodemcu_float_HEAD_date-heure.bin, il s'agit de notre firmware.

  NB : Il est possible que l'icone ne soit pas présente dans ce cas cliquer sur run pipeline au dessus puis selectionner la branche magnet connecté puis create pipeline, cela rendra le firmware disponible

## Flasher le firmware

Pour le flash du firmware, il nous faut installer un outil en suivant ces instructions : https://nodemcu.readthedocs.io/en/master/flash/

Les grandes lignes sont, installer python et pip puis faire un pip install esptool. Sous un gnu/linux de la famille debian ça donne :
```
sudo apt-get install python-pip
pip install esptool
```

Une fois l'outil installé lancer la commande suivante dans le répertoire de notre firmware.
```
# attention le nom du fichier sera different pour vous car il contient une date.
esptool.py --port /dev/ttyUSB0 write_flash -fm dio 0x00000 bin/nodemcu_float_HEAD_20190414-1456.bin
```

**Le lien que je fourni au dessus est la référence officiel le reste ne sont que des indices et peuvent ne pas fonctionner chez vous("Chez moi ça marche." est l'une des phrases phare dans le monde de l'informatique)**

## Configurer le programme

Le programme est composé d'un fichier init.lua et HelloWorld.lua. Le fichier init sert à lancer messages_udp automatiquement, avant cela il est également chargé de se connecter à votre box. Il faudra donc l'éditer pour remplacer les valeurs "your_livebox_name" et "your_livebox_password" par vos informations livebox ou autre point d'accès.

Les valeurs suivantes sont à configurer dans le fichier init.lua:

 SSID = "SSID DE VOTRE BOX"
 PASSWORD = "CLE WPA DE VOTRE BOX"
 TOPICS = {"PRENOM1","PRENOMX"} -- vous pouvez en mettre autant que vous le souhaitez
 MQTT_USERNAME="USERNAME A RELEVER DANS LES DETAILS DU COMPTE MQTTCLOUD"
 MQTT_PASSWORD="MDP A RELEVER AU MEME ENDROIT"
 MQTT_CLOUD_URL="URL A RELEVER AU MEME ENDROIT"
 MQTT_CLOUD_PORT=14441 -- A relever au même endroit attention pas de guillemet pour cette valeur

 Cela donne le fichier init suivant :

 ```lua
print("\n")
print("ESP8266 Started")

local luaFile = {"HelloWorld.lua"}
for i, f in ipairs(luaFile) do
    if file.open(f) then
      file.close()
      print("Compile File:"..f)
      node.compile(f)
   print("Remove File:"..f)
       file.remove(f)
     end
  end
 
 luaFile = nil
 collectgarbage()
 print("Connecting to WiFi access point...")
 -- remplir a partir de cette ligne
 SSID = "Livebox-XXXX"
 PASSWORD = "XXXXYYYYZZZZWWWW"
 TOPICS = {"Celeste","Max"}
 MQTT_USERNAME="username"
 MQTT_PASSWORD="password"
 MQTT_CLOUD_URL="m24.cloudmqtt.com"
 MQTT_CLOUD_PORT=14441
 -- fin de la partie a modifier la suite doit rester intacte
 wifi.setmode(wifi.STATION)
 wifi.sta.config({ssid=SSID, pwd=PASSWORD})
 
 dofile("HelloWorld.lc");

 ```

## Charger le programme

Suivre les instructions officielles ici : https://nodemcu.readthedocs.io/en/master/upload/

Personnellement j'utilise nodemcu_tool c'est un outil simple à utiliser écrit en javascript. Pour l'installer il faut d'abord installer nodejs puis faire un npm install nodemcu_tool.
Pour ceux qui ne connaissent pas le plus simple est peut-être d'installer nodemcu-uploader car il est également en python et s'installe avec pip que l'on avait installé juste avant.
Sous linux cela donne :

```
pip install nodemcu-uploader
nodemcu-uploader upload HelloWorld.lua
nodemcu-uploader upload init.lua
# puis appuyer sur la touche reset du nodemcu
```

L'écran devrait s'initialiser et le nodemcu se brancher à votre box. Vous pourrez découvrir l'ip qui lui a été attribué via l'interface de votre box.

# Preparer l'environnement

* Creer un compte cloudmqtt : https://customer.cloudmqtt.com/instance

* Creer une instance : nom que vous voulez, instance type cat et tag que vous voulez.

* Cliquer sur l'instance et relever : server, user, password et port

* Creer une instance heroku : https://dashboard.heroku.com/apps

* deployer l'application sur heroku : forker le dépo de la webapp BLABLABLA

# Utilisation

Se connecter à la webapp https://connectedmagnet.herokuapp.com/ Choisir Max ou Celeste comme expediteur, entrer un message puis envoyer.


# A venir

* Tester sous windows

